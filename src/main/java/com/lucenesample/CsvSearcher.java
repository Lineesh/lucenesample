package com.lucenesample;

import java.io.IOException;
import java.nio.file.Paths;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

public class CsvSearcher {

	IndexSearcher indexSearcher;
	QueryParser queryParser;
	Query query;

	public CsvSearcher(String indexDirectoryPath) throws IOException {
		Directory indexDirectory = FSDirectory.open(Paths
				.get(indexDirectoryPath));

		IndexWriterConfig config = new IndexWriterConfig(new StandardAnalyzer());
		IndexWriter writer = new IndexWriter(indexDirectory, config);
		IndexReader reader = DirectoryReader.open(writer, false);
		indexSearcher = new IndexSearcher(reader);
		queryParser = new QueryParser(LuceneConstants.CONTENTS,
				new StandardAnalyzer());
	}

	public TopDocs search(String searchQuery) throws IOException,
			ParseException {
		query = queryParser.parse(searchQuery);
		return indexSearcher.search(query, LuceneConstants.MAX_SEARCH);
	}

	public TopDocs search(Query searchQuery) throws Exception {
		return indexSearcher.search(searchQuery, LuceneConstants.MAX_SEARCH);
	}

	public Document getDocument(ScoreDoc scoreDoc)
			throws CorruptIndexException, IOException {
		return indexSearcher.doc(scoreDoc.doc);
	}

	public void close() throws IOException {
		// indexSearcher.close();
		indexSearcher = null;
	}
}