package com.lucenesample;

import java.io.File;
import java.io.FileFilter;

public class CsvFileFilter implements FileFilter {

	public boolean accept(File pathname) {
		return pathname.getName().toLowerCase().endsWith(".csv");
	}
}