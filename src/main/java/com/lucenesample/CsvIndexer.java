package com.lucenesample;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

public class CsvIndexer {

	private IndexWriter writer;
	final static Charset ENCODING = StandardCharsets.UTF_8;

	public CsvIndexer(String indexDirectoryPath) throws Exception {
		// this directory will contain the indexes
		Directory indexDirectory = FSDirectory.open(Paths
				.get(indexDirectoryPath));

		// create the indexer
		IndexWriterConfig config = new IndexWriterConfig(new StandardAnalyzer());
		writer = new IndexWriter(indexDirectory, config);
	}

	public void close() throws Exception {
		writer.close();
	}

	private Document getDocument(File file) throws Exception {
		Document document = new Document();

		Field fileNameField = new TextField(LuceneConstants.FILE_NAME,
				file.getName(), Field.Store.YES);
		document.add(fileNameField);
		Field filePathField = new TextField(LuceneConstants.FILE_PATH,
				file.getCanonicalPath(), Field.Store.YES);
		document.add(filePathField);

		List<String> contents = readFile(file.getAbsolutePath());
		String[] titles = contents.get(0).split(",");
		for (int j = 1; j < contents.size(); j++) {
			String content = contents.get(j);
			String[] values = content.split(",");
			for (int i = 0; i < values.length; i++) {
				Field contentField = new StringField(titles[i], values[i],
						Field.Store.YES);
				document.add(contentField);
			}
		}

		return document;
	}

	private void indexFile(File file) throws Exception {
		System.out.println("Indexing " + file.getCanonicalPath());
		Document document = getDocument(file);

		String fileNameKey = file.getName().substring(0,
				file.getName().indexOf("."));
		Term term = new Term(LuceneConstants.FILE_NAME, fileNameKey);
		Query query = new TermQuery(term);
		writer.deleteDocuments(query);

		writer.addDocument(document);
	}

	private List<String> readFile(String inputFileName) throws IOException {
		Path path = Paths.get(inputFileName);
		return Files.readAllLines(path, ENCODING);
	}

	public int createIndex(String dataDirPath, FileFilter filter)
			throws Exception {
		// get all files in the data directory
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		Resource[] resources = resolver.getResources(dataDirPath + "/**");

		for (Resource resource : resources) {
			indexFile(resource.getFile());
		}
		return writer.numDocs();
	}
}