package com.lucenesample;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TermRangeQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.util.BytesRef;

public class Tester {
	String indexDir = "index";
	String dataDir = "data";
	CsvIndexer indexer;
	CsvSearcher searcher;

	public static void main(String[] args) throws Exception {
		Tester tester = new Tester();
		tester.createIndex();
		//tester.searchUsingTermQuery("id", "1");
		tester.searchUsingRangeQuery("score", "D", "E");
	}

	private void createIndex() throws Exception {
		indexer = new CsvIndexer(indexDir);
		long startTime = System.currentTimeMillis();
		int numIndexed = indexer.createIndex(dataDir, new CsvFileFilter());
		long endTime = System.currentTimeMillis();
		indexer.close();
		System.out.println(numIndexed + " File indexed, time taken: "
				+ (endTime - startTime) + " ms");
	}

	private void searchUsingTermQuery(String title, String word)
			throws Exception {
		searcher = new CsvSearcher(indexDir);
		long startTime = System.currentTimeMillis();
		// create a term to search file name
		Term term = new Term(title, word);
		// create the term query object
		Query query = new TermQuery(term);
		// do the search
		TopDocs hits = searcher.search(query);
		long endTime = System.currentTimeMillis();

		System.out.println(hits.totalHits + " documents found. Time :"
				+ (endTime - startTime) + "ms");
		for (ScoreDoc scoreDoc : hits.scoreDocs) {
			Document doc = searcher.getDocument(scoreDoc);
			System.out.println("File: " + doc.get(LuceneConstants.FILE_PATH));
		}
		searcher.close();
	}

	private void searchUsingRangeQuery(String field, String min, String max)
			throws Exception {
		searcher = new CsvSearcher(indexDir);
		long startTime = System.currentTimeMillis();

		// TermRangeQuery query = TermRangeQuery.newStringRange(field, min, max,
		// true, true);
		TermRangeQuery query = new TermRangeQuery(field, new BytesRef(min),
				new BytesRef(max), true, true);
		TopDocs hits = searcher.search(query);
		long endTime = System.currentTimeMillis();

		System.out.println(hits.totalHits + " documents found. Time :"
				+ (endTime - startTime) + "ms");
		for (ScoreDoc scoreDoc : hits.scoreDocs) {
			Document doc = searcher.getDocument(scoreDoc);
			System.out.println("File: " + doc.get(LuceneConstants.FILE_PATH));
		}
		searcher.close();
	}

}
