
To run:
  clone the repository
  mvn build
  Run the Tester.java

Description:
This application shows the basic usage of Lucene. The input files are kept in the "data" directory in classpath. 

The application reads all the ".csv" files in the "data" directory, and indexes the content. It expects the column heading in the first line. The column headings of these files don't need to match.

Once the files are indexed, we can run queries using the index.